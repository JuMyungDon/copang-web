import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardCopangInfo from '~/components/card-copang-info'
Vue.component('CardCopangInfo', CardCopangInfo)
